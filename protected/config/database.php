<?php

// This is the database connection configuration.
return array(
	// 'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	'connectionString' => 'mysql:host=172.21.0.1:33061;dbname=influcencer_marketing_app_v1',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'example2',
	'charset' => 'utf8',
	'tablePrefix' => 'im_'
);
