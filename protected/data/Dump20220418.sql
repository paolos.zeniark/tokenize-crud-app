-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: influcencer_marketing_app_v1
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `im_influencer`
--

DROP TABLE IF EXISTS `im_influencer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `im_influencer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `seeding_platform` tinyint(4) NOT NULL,
  `followers` int(11) NOT NULL,
  `handle_tiktok` varchar(128) DEFAULT NULL,
  `handle_instagram` varchar(128) DEFAULT NULL,
  `handle_youtube` varchar(128) DEFAULT NULL,
  `hyperlink_instagram` text,
  `hyperlink_tiktok` text,
  `hyperlink_youtube` text,
  `following_tier` tinyint(4) NOT NULL,
  `demographic` tinyint(4) NOT NULL,
  `segment` tinyint(4) NOT NULL,
  `notes` text,
  `added_by_user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_im_Influencer_1_idx` (`added_by_user_id`),
  CONSTRAINT `fk_im_Influencer_1` FOREIGN KEY (`added_by_user_id`) REFERENCES `im_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `im_influencer`
--

LOCK TABLES `im_influencer` WRITE;
/*!40000 ALTER TABLE `im_influencer` DISABLE KEYS */;
INSERT INTO `im_influencer` VALUES (1,'Jane','qweqweqwe',1,12,'JaneDuhTK','JaneDuhIns','JaneDuhYT','','','',2,2,2,'zen1ark!@#',28,3,'2022-04-18 14:33:14','2022-04-18 14:33:28'),(2,'weqwe','qweqweqwe',3,12,'eqweqw','eqweqw','eqweq','https://www.google.com/','https://www.google.com/','https://www.google.com/',1,1,2,'',28,3,'2022-04-18 14:36:29','2022-04-18 14:41:03');
/*!40000 ALTER TABLE `im_influencer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `im_user`
--

DROP TABLE IF EXISTS `im_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `im_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `im_user`
--

LOCK TABLES `im_user` WRITE;
/*!40000 ALTER TABLE `im_user` DISABLE KEYS */;
INSERT INTO `im_user` VALUES (27,'JM','576f1f5e7e5b5ac83b440c134b9fc89cd3666a21','1650263538','jm.zeniark@gmail.com'),(28,'Reg','f00cf39c508fb90f3598a026feac1edb63d973cd','1650263569','reg.zeniark@gmail.com');
/*!40000 ALTER TABLE `im_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-18 14:43:01
