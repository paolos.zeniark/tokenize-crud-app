<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php $this->widget('Flash', array(
				'flashes' => Yii::app()->user->getFlashes()
			)); ?>
		</div>
		<div class="col-md-offset-3 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>View</strong></div>
				<div class="panel-body">
					<table class="table table-condensed table-bordered table-striped">
						<colgroup>
							<col style="width: 30%;">
							<col style="width: 70%;">
						</colgroup>
						<tr>
							<th>Date Created</th>
							<td><?php echo $model->date_created; ?></td>
						</tr>
						<tr>
							<th>Added By</th>
							<td><?php echo $model->addedByUser->username; ?></td>
						</tr>
						<tr>
							<th>Full Name</th>
							<td><?php echo $model->first_name . ' ' . $model->last_name; ?></td>
						</tr>
						<tr>
							<th>Seeding Plaform</th>
							<td><?php echo Influencer::getSeedingPlatformLabel($model->seeding_platform); ?></td>
						</tr>
						<tr>
							<th>Followers</th>
							<td><?php echo $model->followers; ?></td>
						</tr>
						<tr>
							<th>TikTok Handle</th>
							<td><?php echo $model->handle_tiktok; ?></td>
						</tr>
						<tr>
							<th>Instgram Handle</th>
							<td><?php echo $model->handle_instagram; ?></td>
						</tr>
						<tr>
							<th>Youtube Handle</th>
							<td><?php echo $model->handle_youtube; ?></td>
						</tr>

						<tr>
							<th>TikTok Hyperlink</th>
							<td><?php echo $model->hyperlink_tiktok; ?></td>
						</tr>
						<tr>
							<th>Instgram Hyperlink</th>
							<td><?php echo $model->hyperlink_instagram; ?></td>
						</tr>
						<tr>
							<th>Youtube Hyperlink</th>
							<td><?php echo $model->hyperlink_youtube; ?></td>
						</tr>

						<tr>
							<th>Following Tier</th>
							<td><?php echo Influencer::getFollowingTierLabel($model->following_tier); ?></td>
						</tr>
						<tr>
							<th>Demographic</th>
							<td><?php echo Influencer::getDemographicLabel($model->demographic); ?></td>
						</tr>
						<tr>
							<th>Segment</th>
							<td><?php echo Influencer::getSegmentLabel($model->segment); ?></td>
						</tr>
						<tr>
							<th>Notes</th>
							<td><?php echo $model->notes; ?></td>
						</tr>
						<tr>
							<td colspan="2">
								<?php echo CHtml::link('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
								<?php echo CHtml::link('Delete', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?")']); ?>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

